<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBoardPiecesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('board_pieces')) {
            Schema::create('board_pieces', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('Board_id')->unsigned();
                $table->foreign('Board_id')->references('id')->on('boards');
                $table->integer('Piece_id')->unsigned();
                $table->foreign('Piece_id')->references('id')->on('pieces');
                $table->integer('x');
                $table->integer('y');
                $table->text('commands');


            });
        }
    }

    public function down()
    {
        Schema::dropIfExists('board__pieces');
    }
}
