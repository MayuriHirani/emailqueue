<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMovesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('moves', function (Blueprint $table) {
            $table->integer('Board_id')->unsigned();
            $table->foreign('Board_id')->references('id')->on('boards');
            $table->integer('Piece_id')->unsigned();
            $table->foreign('Piece_id')->references('id')->on('pieces');
            $table->text('commands');
            $table->timestamps();
        });

        Schema::table('moves', function (Blueprint $table) {
            $table->softDeletes();

        });
    }

    public function down()
    {
        Schema::dropIfExists('moves');
    }
}
