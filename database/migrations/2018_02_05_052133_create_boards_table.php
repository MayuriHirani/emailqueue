<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBoardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('boards', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('Rows');
            $table->integer('Columns');
            $table->integer('Game_id')->unsigned();
            $table->foreign('Game_id')->references('id')->on('games');

        });
    }

    public function down()
    {
        Schema::dropIfExists('boards');
    }
}
