<?php

use Illuminate\Database\Seeder;


class PiecesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $boards = App\Board::all();
        $totalPieces = mt_rand(1, 5);


        foreach ($boards as $board) {
            $totalPieces = mt_rand(1, 5);
            echo $totalPieces;
            for ($piece = 1; $piece <= 2; $piece++) {
                $x = mt_rand(1, $board->Rows);
                $y = mt_rand(1, $board->Columns);

                $pieces = App\Piece::all();

                echo "  x=>" . $x . " y=>" . $y;
                if ($pieces->isNotEmpty()) {

                    $pieceExitst = App\Piece::where('x', $x)->where('y', $y)->first();
                    if (!$pieceExitst) {
                        $newPiece = new App\piece();
                        $newPiece->x = $x;
                        $newPiece->y = $y;
                        $newPiece->save();

                        factory(App\Board_Piece::class)->create(['Board_id' => $board->id, 'Piece_id' => $newPiece->id, 'x' => $x, 'y' => $y]);

                    } else {
                        $exists = App\Board_Piece::where('x', $x)->where('y', $y)->where('Board_id', $board->id)->first();
                        if (!$exists) {
                            factory(App\Board_Piece::class)->create(['Board_id' => $board->id, 'Piece_id' => $pieceExitst->id, 'x' => $x, 'y' => $y]);
                        }

                    }
                } else {
                    $newPiece = new App\Piece();
                    $newPiece->x = $x;
                    $newPiece->y = $y;
                    $newPiece->save();
                    factory(App\Board_Piece::class)->create(['Board_id' => $board->id, 'Piece_id' => $newPiece->id, 'x' => $x, 'y' => $y]);
                }
            }
        }

        }
    }
