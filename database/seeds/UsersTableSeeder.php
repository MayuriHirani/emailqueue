<?php

use App\Piece;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
        protected $boardId;
        protected  $pieceId;
        protected  $boardRows;
        protected  $boardColumns;

    public function run()
    {
        factory(App\User::class,1)->create();
    }


}
