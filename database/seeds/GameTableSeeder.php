<?php

use Illuminate\Database\Seeder;

class GameTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       $users=App\User::all();
       foreach ($users as $user)
       {
           for($i=1 ;$i<=1; $i++){

               $game = new App\Game();
               $game->is_active=true;
               $game->User_id=$user->id;
               $game->save();
           }
       }
    }
}
