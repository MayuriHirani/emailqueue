<?php

use Illuminate\Database\Seeder;

class BoardsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       $games=App\Game::all();
       foreach ($games as $game)
       {
           for($i=1;$i<=1;$i++){

               $row=mt_rand(4,5);
               $column=mt_rand(4,5);
               $board=new App\Board();
               $board->Rows=$row;
               $board->Columns=$column;
               $board->Game_id=$game->id;
               $board->save();


           }
       }


    }
}
