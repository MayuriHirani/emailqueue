<?php

use Faker\Generator as Faker;

$factory->define(App\Board_Piece::class, function (Faker $faker)
{
    return [
        'commands'=>implode(',',$faker->randomElements(['left','right','up','down'],mt_rand(1,4))),
    ];
});
