<?php



Route::get('/', function () {
    return view('welcome');
});
//
Auth::routes();
Route:: get('/auth/{provider}','Auth\authController@redirect');
Route:: get('/auth/{provider}/callback','Auth\authController@callback');
Route::get('/user/verify/{token}', 'verify@checkMail');
Route::get('/verify',function (){
    return view('notVerified');
});
Route::any('/home', 'HomeController@index')->middleware('auth')->name('home');

Route::any('/adminPage',function (){

    return view('/adminPage');

})->middleware('auth')->middleware('verifyEmail')->middleware('CheckRole')->name('admin');
//Route::get('/viewBoard','gameController@showBoard')->middleware('auth')->name('main');
Route::post('/moves','gameController@move')->middleware('auth');

Route::get('/boardGame','gameController@storeGame')->middleware('auth');
/*Route::any('/boardGame','gameController@storeGame')->middleware('auth')->middleware('verifyEmail')->name('main');*/
Route::post('/storeBoard','gameController@storeBoard')->middleware('auth');
Route::get('/showBoard','gameController@showBoard')->middleware('auth')->name('main');
//Route::post('/moves','gameController@userView')->middleware('auth');
Route::post('/moves','gameController@move')->middleware('auth');



