<?php

namespace App;
use App;
use Illuminate\Database\Eloquent\Model;

class Game extends Model
{
    protected  $table='games';
    protected $fillable =array( 'is_active','User_id' );
    public $timestamps=false;

    public function board()
    {
        return $this->hasOne('App\Board','Game_id');
    }
    public function  users()
    {
        return $this->belongsTo('App\User','User_id');
    }

}
