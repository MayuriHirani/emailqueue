<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Board_Piece extends Model
{
    protected $table='board_pieces';
    protected $fillable=array('Board_id','Piece_id','x','y','commands');

    public $timestamps=false;
    public function board()
    {
        return $this->belongsTo('App\Board','Board_id');
    }
    public function  piece()
    {
        return $this->belongsTo('App\Piece','Piece_id');
    }
}
