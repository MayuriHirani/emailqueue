<?php

namespace App;
use DB;
use App\Board_Game;
use App\Board_Piece;

use App\piece;

use App\board;

class functions
{
    public $board =[];
    public $piece=[];
    public $count=0;
    public $moves = ["left","right","down","up"];

    public function createPiece($x,$y){
        $piece['x']=$x;
        $piece['y']=$y;
    }
    public function getPiece()
    {
        $finalPiece=DB::table('pieces')->orderby('id','DESC')->first();
        $piece=[ 'id'=>$finalPiece->id,
            'x'=>$finalPiece->xCoordinate,
            'y'=>$finalPiece->yCoordinate];

        return $piece;
    }

    public function up()
    {
        $piece=$this->getPiece();
        if(!($piece['x'] == 1)) {
            $piece['x'] -= 1;
            DB::update('update pieces set xCoordinate = ?,yCoordinate=? where id=?',[ $piece['x'],$piece['y'],$piece['id'] ]);
        }
        else
        {
            die("cant move up");
        }
    }
    public function down()
    {
        $board = DB::table('boards')->orderby('Board_id','DESC')->first();
        $piece=$this->getPiece();
        $rowNum=$board->noOfRows;
        if($piece['x'] == $rowNum)
        {
            die("can't move down");
        }
        $piece['x']+=1;
        DB::update('update pieces set xCoordinate = ?,yCoordinate=? where id=?',[ $piece['x'],$piece['y'],$piece['id'] ]);

    }
    public function left()
    {
        $board = DB::table('boards')->orderby('Board_id','DESC')->first();
        $piece=$this->getPiece();
        if($piece['y'] == 1)
        {
            die("can't move left");
        }
        $piece['y']-=1;
        DB::update('update pieces set xCoordinate = ?,yCoordinate=? where id=?',[ $piece['x'],$piece['y'],$piece['id'] ]);
    }
    public function right()
    {
        $board = DB::table('boards')->orderby('Board_id','DESC')->first();
        $piece=$this->getPiece();
        $colNum=$board->noOfColumns;
        if(!($piece['y'] == $colNum))
        {
            $piece['y'] += 1;
            DB::update('update pieces set xCoordinate = ?,yCoordinate=? where id=?',[ $piece['x'],$piece['y'],$piece['id'] ]);
        }
        else
        {
            die("can't move right!!");
        }
    }
    public function autoMove($moves)
    {
        $moveCount=file_get_contents('move.txt');
        if($moveCount < count($moves))
        {
            if($moves[$moveCount] == 'up')
            {
                $this->up();
            }
            if($moves[$moveCount] == 'down')
            {
                $this->down();

            }
            if($moves[$moveCount] == 'left')
            {
                $this->left();

            }
            if($moves[$moveCount] == 'right')
            {
                $this->right();

            }
            $moveCount +=1;
            file_put_contents('move.txt', $moveCount);
        }
        else
        {
            die("can't move");

        }
        header("Refresh:3;");
    }

}

?>
