<?php

namespace App\Jobs;

use App\Mail\Verification;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use Mail;


class SendVarificationEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected  $user;
    public function __construct($user)
    {
        $this->user=$user;
        $this->handle();
    }
    public function handle()
    {

        $email=new Verification($this->user);
      Mail::to($this->user->email)->send($email);

    }
}
