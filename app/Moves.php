<?php

namespace App;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Moves extends Model
{

    protected  $table='moves';
    protected $fillable =array( 'Board_id','Piece_id','commands' );

    protected $dates=['deleted_at'];
    public function board()
    {
        return $this->belongsTo('Board','Board_id');
    }
    public function  piece()
    {
        return $this->belongsTo('Piece','Piece_id');
    }
}
