<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    public $user;

    protected $fillable = [
        'name', 'email', 'password','provider','provider_id','verified_at','verification_token','role'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function game()
    {
        return $this->hasMany('App\Game','User_id');
    }
}
