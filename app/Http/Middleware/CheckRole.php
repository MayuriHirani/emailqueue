<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CheckRole
{
    public function handle(Request $request, Closure $next)
    {
        $user=Auth::user();
        if($user->role != 'Admin')
        {
            $request->session()->flash('notAdmin','You can not access this link');
            return redirect(route('home'));

        }
        return $next($request);
    }
}
