<?php

namespace App\Http\Middleware;
use Closure;
use Illuminate\Support\Facades\Auth;

class verifyEmail
{
    public function handle($request, Closure $next)
    {
        $users = Auth::user();
        if($users->verified_at == null)
        {
            Auth::logout();
            return redirect('/verify');
        }
        return $next($request);
    }

}
