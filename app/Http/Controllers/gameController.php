<?php

namespace App\Http\Controllers;
use App\Moves;
use Illuminate\Http\Request;
use DB;
use Auth;
use App\Game;
use App\Piece;
use App\Board;
use App\Board_Piece;
use App\User;


class gameController extends Controller
{
    public $row;
    public $col;
    public $errors=[];
    public function storeGame()
    {
        $game = new Game;
        $user = Auth::user();
        $game->is_active = true;
        $game->User_id = $user->id;
        $game->save();
        return view('createBoard');

    }
    public function storeBoard(Request $request)
    {
        $game = new Game;
        $user = Auth::user();
        $game->is_active = true;
        $game->User_id = $user->id;
        $game->save();
        $board = new Board;
        $pieces = $request->input('pieces');
        //Insert into game:

       // $currentGame = Game::where('is_active', 1)->first();
        $row = $request->input('row');
        $col = $request->input('col');

        foreach ($pieces as $piece) {
            if ($piece['x'] > $row || $piece['y'] > $col) {

                $request->session()->flash('error', 'enter valid pieces');
                return redirect("/showBoard");
            }

        }
        $board->Rows = $row;
        $board->Columns = $col;
        $board->Game_id = $game->id;
        $board->save();
        foreach ($pieces as $piece) {
            $oldPiece = Piece::firstOrCreate(['x' => $piece['x'], 'y' => $piece['y']]);
            //dd($oldPiece);
            $boardPiece = new Board_Piece;
            $boardPiece->Board_id = $board->id;
            $boardPiece->Piece_id = $oldPiece->id;
            $boardPiece->x = $oldPiece->x;
            $boardPiece->y = $oldPiece->y;
            $boardPiece->commands = $piece['commands'];
            $boardPiece->save();
        }

        return redirect('/showBoard');
    }

    public function showBoard(Request $request)
    {

        //dd($gam
        $user = Auth::user();
        $game = $user->game()->where('is_active',1)->get()->first();
        //dd($game);
        if($request->input('move')=='NewGame') {


            if ($game) {
                $userBoard = $game->board()->first();
                $row = $userBoard->Rows;
                $col = $userBoard->Columns;
                $newPieces = $userBoard->board_piece()->whereNotNull('commands')->get();
                //   $newPieces = Board_Piece::where('board_id', $userBoard->id)->whereNotNull('commands')->get();
                return view('boardView', ['row' => $row, 'col' => $col, 'gameid' => $userBoard->Game_id, 'pieces' => $newPieces]);
            } else {

                $request->session()->flash('Error','all game is over please create new game');
                return view('createBoard');
            }
        }
        else
        {
            if ($game) {
                $userBoard = $game->board()->first();
                $row = $userBoard->Rows;
                $col = $userBoard->Columns;
                $newPieces = $userBoard->board_piece()->whereNotNull('commands')->get();
                //   $newPieces = Board_Piece::where('board_id', $userBoard->id)->whereNotNull('commands')->get();
                return view('boardView', ['row' => $row, 'col' => $col, 'gameid' => $userBoard->Game_id, 'pieces' => $newPieces]);
            }
            else
            {
                $request->session()->flash('Error','all game is over please create new game');
                return view('createBoard');
            }

        }

    }

    public function move(Request $request)
    {
        $user = Auth::user();
        $game = $user->game()->where('is_active',1)->get()->first();
        if(!$game)
        {
            $request->session()->flash('over','please create new game');
            return redirect('/showBoard');
        }

        $userBoard = Board::where('Game_id', $game->id)->first();
        $row = $userBoard->Rows;
        $col = $userBoard->Columns;
        $newPieces = Board_Piece::where('board_id', $userBoard->id)->whereNotNull('commands')->get();

        foreach ($newPieces as $piece) {
            $count = 0;
            $pieceMoves = explode(",", $piece->commands);
            $pieceMove = array_shift($pieceMoves);
            $newCommands = implode(",", $pieceMoves);

            if (!empty($pieceMove)) {
                $count++;
                if ($pieceMove == "up") {
                    if (!($piece->x == 1)) {
                        $piece->x -= 1;

                    } else {
                        echo
                        Board_Piece::where('Piece_id', $piece->Piece_id)->where('Board_id', $userBoard->id)
                            ->update(['commands'=>'']);
                        echo '<script language="javascript"> alert("can not move up") </script>';
                    }

                }
                if ($pieceMove == "down") {
                    if (!($piece->x == $row)) {
                        $piece->x += 1;
                    } else
                    {
                        Board_Piece::where('Piece_id', $piece->Piece_id)->where('Board_id', $userBoard->id)
                            ->update(['commands'=>'']);
                        echo '<script language="javascript"> alert("can not move down") </script>';
                    }

                }
                if ($pieceMove == "left") {
                    if (!($piece->y == 1))
                    {
                        $piece->y -= 1;
                    }
                    else
                    {
                        Board_Piece::where('Piece_id', $piece->Piece_id)->where('Board_id', $userBoard->id)
                            ->update(['commands'=>'']);
                        echo '<script language="javascript"> alert("can not move left") </script>';
                    }

                }
                if ($pieceMove == "right") {
                    if (!($piece->y == $col)) {
                        $piece->y += 1;

                    } else {
                        Board_Piece::where('Piece_id', $piece->Piece_id)->where('Board_id', $userBoard->id)
                            ->update(['commands'=>'']);
                        echo '<script language="javascript"> alert("can not move right") </script>';
                    }

                }
                $move = new Moves;
                $move->Board_id = $userBoard->id;
                $move->Piece_id = $piece->id;
                $move->commands = $pieceMove;
                $move->save();
            }
            Board_Piece::where('Piece_id', $piece->Piece_id)->where('Board_id', $userBoard->id)
                ->update(['x' => $piece->x, 'y' => $piece->y, 'commands' => $newCommands]);
        }
        if (empty($count))
        {
            $request->session()->flash('over','Moves Over');
            Game::where('id', $game->id)->update(['is_active' => false]);
            //return redirect('/showBoard');
        }

        else
        {

            echo "<script> setTimeout( function (){  window.location.reload(1); },3000); </script>";
            //redirect ('/home');
        }
        $userBoard = Board::where('Game_id', $game->id)->first();
        $row = $userBoard->Rows;
        $col = $userBoard->Columns;
        $gameid = $userBoard->Game_id;
        $newPieces = Board_Piece::where('board_id', $userBoard->id)->whereNotNull('commands')->get();

        return view('boardView', ['row' => $row, 'col' => $col, 'gameid' => $gameid, 'pieces' => $newPieces]);
    }

}