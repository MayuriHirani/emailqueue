<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;


class verify extends Controller
{
    public function checkMail(Request $request, $token)
    {
        if ($user = User::where('verified_at',null)->where('verification_token',$token)->first())
        {
            $dateAtCreated=$user->created_at;
            $dateAtVerified=Date('Y-m-d H:i:s');
            $newDate=date_create($dateAtVerified);
            $dateDiff=date_diff($newDate,$dateAtCreated);

            if($dateDiff->i > 60)
            {
                User::where('verification_token',$token)->delete();
                $request->session()->flash('timeout','Please register link is expired');
                return redirect('/register');
            }
            Auth::logout();
            $user->verified_at= $newDate;
            $user->save();

        }
        return redirect('home');
    }
}