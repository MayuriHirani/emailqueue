<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Board extends Model
{
    public $timestamps=false;
    protected $table='boards';

    protected $fillable=array('Rows','Columns','Game_id');

    public function game()
    {
        return $this->belongsTo('App\Game','Game_id');
    }
    public function board_piece()
    {
        return $this->hasMany('App\Board_Piece','Board_id');
    }
    public function moves()
    {
        return $this->hasMany('App\Moves','Board_id');
    }
}
