<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Piece extends Model
{
    protected  $table='pieces';
    protected $fillable=array('x','y');
    public $timestamps=false;


    public function board_piece()
    {
        return $this->belongsTo('App\Board_Piece','Piece_id');
    }
    public function moves()
    {
        return $this->hasMany('App\Moves','Piece_id');
    }

}
