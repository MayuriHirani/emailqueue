@extends('layouts.app')

@section('content')

@if(session()->has('error'))
    <h3 style="color: #0000F0">{{ session()->get('error') }}</h3>
@endif
@if(session()->has('Error'))
    <h3 style="color: #0000F0">{{ session()->get('Error') }}</h3>
@endif

<script type="text/javascript">

    var count=0;
    function getPieces()
    {
        var x="pieces[" + count +"][x]";
        var y="pieces[" + count +"][y]";
        var commands="pieces[" + count +"][commands]";
        document.getElementById('place').innerHTML += "<br>Enter x:<input type='text' name='" + x + "'/><br/>" +
                                                    "Enter y:<input type='text' name='" + y + "'/><br/>" +
                                                    "Enter commands:<input type='text' name='" + commands + "'/><br/>";
        count+=1;

    }

</script>
<html>
    <body>
<center>
    <form name="f1" method="post" action="/storeBoard" >
        {{ csrf_field() }}
        <fieldset name="createBoard" >
            <legend>
                Create New Board
            </legend>

            Enter Rows : <input type="text" name="row" /><br/>
            Enter Columns : <input type="text" name="col" /><br/>

        </fieldset>
        <fieldset name="createPiece" >
            <legend>Create Pieces:</legend>
            <input type="button" value="addmore" onclick="getPieces()"/>

                <span id="place"> </span>

        </fieldset><br/>
        <input type="submit" name="submit" value="Start"/>
    </form>

</center>
    </body>


</html>
@endsection

