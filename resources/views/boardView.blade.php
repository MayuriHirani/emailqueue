
@extends('layouts.app')

@section('board')


    <html>
    <head>
        <style>
            .board td {
                height:40px;
                width:40px;
            }
        </style>
    </head>
<center>


    @if(session()->has('over'))
        <h3 style="color: #0000F0">{{ session()->get('over') }}</h3>
    @endif
    <table border ='1'  class="board">
    <h4> game: {{$gameid}}</h4>
    @for($rows = 1; $rows <= $row;$rows++)
        <tr>
            @for($column = 1;$column <= $col ;$column++)
                <?php $flag=false; ?>
                @foreach($pieces as $piece)
                    @if($rows==$piece->x && $column==$piece->y)
                            <?php $flag=true; ?>
                        @break
                    @else
                            <?php $flag=false; ?>
                    @endif
                @endforeach
                @if($flag == true)
                        <td>1</td>
                @else
                        <td></td>
                @endif
            @endfor
        </tr>
@endfor
</table>

<form name="move" method="post" action="/moves">
    {{ csrf_field() }}

    <input  type="submit" name="move" value="StartMove"/>

</form>
<form method="get" action = "/showBoard">
    <input type="submit" name="move" value="NewGame">

</form>
</center>
</body> </html>
    @endsection
