@extends('layouts.app')

@section('content')
    @if(session()->has('Error'))
        <h3 style="color:blue"><center> {{ session()->get('Error')}} </center></h3>

    @endif

    @if(session()->has('notAdmin'))
        <h3 style="color:blue"><center> {{ session()->get('notAdmin')}} </center></h3>

    @endif
    @if(session()->has('Error'))
        <h3 style="color: #0000F0">{{ session()->get('over') }}</h3>
    @endif

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">welcome</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div>
                        @php
                            $user=Auth::user();
                            $game = $user->game()->where('is_active',1)->get()->first();
                        @endphp
                        @if($game)

                            @foreach($games as $game)

                                <li><a href="{{ route('main') }}">game: {{$game->id}}</a></li>

                            @endforeach
                        @else
                            <li><a href="{{ route('main') }}"> create new game </a> </li>
                            <a href="{{ route('admin') }}">Admin</a>
                        @endif


                </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
